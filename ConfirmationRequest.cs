﻿using System;
using System.Runtime.InteropServices;

namespace UBHelper {
    /// <summary>
    /// Helper class for working with confirmation requests. These are the ingame
    /// popups that require a user to click yes/no for example.
    /// </summary>
    public static unsafe class ConfirmationRequest {
        internal static void Enable() {
            P.ClientUISystem__Handle_Character__ConfirmationRequest.Setup(new P.def_ClientUISystem__Handle_Character__ConfirmationRequest(Hook_ClientUISystem__Handle_Character__ConfirmationRequest));
        }

        internal static void Disable() {
            P.ClientUISystem__Handle_Character__ConfirmationRequest.Remove();
            Decal.Adapter.CoreManager.Current.RenderFrame -= Core_RenderFrame;
        }

        /// <summary>
        /// Raised when a confirmation request (yes/no popup) is wanting to be shown on the client
        /// </summary>
        public static event EventHandler<ConfirmationRequestEventArgs> ConfirmationRequestEvent;

        /// <summary>
        /// Holds information about a confirmation request.  Setting either ClickYes or ClickNo
        /// to true will skip showing the dialog to the user, and perform that action.  If both
        /// ClickYes and ClickNo are set to the default values of false, nothing will happen
        /// and the confirmation request will be shown to the user as normal.
        /// </summary>
        public class ConfirmationRequestEventArgs : EventArgs {
            /// <summary>
            /// The type of confirmation request.
            /// </summary>
            public ConfirmationType Confirm { get; internal set; }
            /// <summary>
            /// The text that would be shown to the user
            /// </summary>
            public string Text { get; internal set; }
            /// <summary>
            /// Set to true to automatically click yes before the popup is shown to the user
            /// </summary>
            public bool ClickYes { get; set; }
            /// <summary>
            /// Set to true to automatically click no before the popup is shown to the user.
            /// </summary>
            public bool ClickNo { get; set; }
        }
        internal static int Hook_ClientUISystem__Handle_Character__ConfirmationRequest(ConfirmationType confirm, int context, int userData) {
            string fff = Marshal.PtrToStringAnsi((IntPtr)(*(int*)userData + 0x14), (*(int*)(*(int*)userData + 0x08)) - 1);

            ConfirmationRequestEventArgs args = new ConfirmationRequestEventArgs {
                Confirm = confirm,
                Text = Core.PStringBase_char_read((int*)userData),
                ClickYes = false,
                ClickNo = false
            };
            ConfirmationRequestEvent?.Invoke(null, args);
            if (args.ClickYes || args.ClickNo) {
                ConfirmationRequest.confirm = confirm;
                ConfirmationRequest.context = context;
                ConfirmationRequest.accepted = (args.ClickYes ? 1 : 0);
                cReqReceivedAt = DateTime.UtcNow;
                Decal.Adapter.CoreManager.Current.RenderFrame += Core_RenderFrame;
                return 1;
            }
            return P.Call_ClientUISystem__Handle_Character__ConfirmationRequest(confirm, context, userData);
        }

        internal static DateTime cReqReceivedAt = DateTime.MinValue;
        internal static int context, accepted;
        internal static ConfirmationType confirm;
        internal static void Core_RenderFrame(object sender, EventArgs e) {
            if (DateTime.UtcNow - cReqReceivedAt > TimeSpan.FromMilliseconds(250)) {
                if (accepted == 0)
                    Core.DecrementBusyCount();
                P.Call_CM_Character__Event_ConfirmationResponse(confirm, context, accepted);
                Decal.Adapter.CoreManager.Current.RenderFrame -= Core_RenderFrame;
            }
        }
    }
}
