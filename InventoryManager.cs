using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace UBHelper {
    /// <summary>
    /// Helper class for working with your inventory
    /// </summary>
    public static unsafe class InventoryManager {


        #region GetInventory
        /// <summary>
        /// Gets a list of itemIds from your inventory, filterable by wield and inventory location
        /// </summary>
        /// <param name="inventory">ref inventory array to fill with ids</param>
        /// <param name="invType">inventory location</param>
        /// <param name="loc">wield location</param>
        public static void GetInventory(ref List<int> inventory, GetInventoryType invType, Weenie.INVENTORY_LOC loc = Weenie.INVENTORY_LOC.NONE_LOC) {
            inventory.Clear();
            int objInventory;
            try { objInventory = *(int*)(*(int*)(Core.CPhysics + 0x12C) + 0x50); } catch { return; }
            int thisIDListNode;
            if ((invType & GetInventoryType.MainPack) != 0) {
                thisIDListNode = *(int*)(objInventory + 0x10);
                while (thisIDListNode != 0) {
                    inventory.Add(*(int*)thisIDListNode);
                    thisIDListNode = *(int*)(thisIDListNode + 0x08);
                }
            }
            if ((invType & (GetInventoryType.Containers | GetInventoryType.AllPacks)) != 0) {
                thisIDListNode = *(int*)(objInventory + 0x28);
                int pack_num = 0;
                int cthisIDListNode;
                while (thisIDListNode != 0) {
                    pack_num++;
                    int thisContainerID = *(int*)thisIDListNode;
                    Weenie container = new Weenie(thisContainerID);
                    if (*container.ObjInventory != 0) {
                        if ((invType & GetInventoryType.Containers) != 0) inventory.Add(thisContainerID);
                        if (((int)invType & 1 << pack_num) != 0) {
                            cthisIDListNode = *(int*)(*container.ObjInventory + 0x10);
                            while (cthisIDListNode != 0) {
                                inventory.Add(*(int*)cthisIDListNode);
                                cthisIDListNode = *(int*)(cthisIDListNode + 0x08);
                            }
                        }
                    }
                    thisIDListNode = *(int*)(thisIDListNode + 0x08);
                }
            }
            if(loc != Weenie.INVENTORY_LOC.NONE_LOC) {
                thisIDListNode = *(int*)(objInventory + 0x40);
                while (thisIDListNode != 0) {
                    if ((*(Weenie.INVENTORY_LOC*)(thisIDListNode + 0x08) & loc) != 0) inventory.Add(*(int*)(thisIDListNode + 0x04));
                    thisIDListNode = *(int*)(thisIDListNode + 0x10);
                }
            }
            inventory.RemoveAll(x => (x == 0));
        }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public enum GetInventoryType {
            MainPack = 0x00000001,
            Pack1 = 0x00000002, Pack2 = 0x00000004, Pack3 = 0x00000008, Pack4 = 0x00000010,
            Pack5 = 0x00000020, Pack6 = 0x00000040, Pack7 = 0x00000080, Pack8 = 0x00000100,
            AllPacks = 0x000001FE,
            Containers = 0x00000200,
            AllItems = 0x000001FF,
            Everything = 0x000003FF
        }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member

        #endregion
        /// <summary>
        /// Checks if a door weenie is open
        /// </summary>
        /// <param name="weenie_id">weenie id of the door to check</param>
        /// <returns>true if door is open</returns>
        public static bool IsDoorOpen(int weenie_id) {
            try { return (*(int*)(new Weenie(weenie_id).PhysicsPtr + 0xA8) & 0x04) == 0x04; } catch { return false; }
        }
    }

#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public unsafe class Weenie {
        public int WeeniePtr { get; internal set; }
        public int Id { get { try { return *(int*)(WeeniePtr + 0x08); } catch { return 0; } } }
        public int* ObjInventory { get { try { return (int*)(WeeniePtr + 0x50); } catch { return (int*)0; } } }
        public bool Valid { get { try { return *(int*)(WeeniePtr + 0x54) == 1; } catch { return false; } } }
        public bool SellState { get { try { return *(int*)(WeeniePtr + 0x74) == 1; } catch { return false; } } }
        public bool TradeState { get { try { return *(int*)(WeeniePtr + 0x78) == 1; } catch { return false; } } }
        public int PhysicsPtr { get { try { return *(int*)(WeeniePtr + 0x94); } catch { return 0; } } }
        public string Name { get { try { return Core.PStringBase_char_read((int*)(WeeniePtr + 0x9C)); } catch { return ""; } } }
        public string GetName(NameType _nameType = NameType.NAME_APPROPRIATE) => WeeniePtr == 0 ? "ERROR" : Marshal.PtrToStringAnsi((IntPtr)P.Call_ACCWeenieObject__GetObjectName(WeeniePtr, _nameType));
        public int WCID { get { try { return *(int*)(WeeniePtr + 0xA4); } catch { return 0; } } }
        public int Icon { get { try { return *(int*)(WeeniePtr + 0xA8); } catch { return 0; } } }
        public int IconOverlay { get { try { return *(int*)(WeeniePtr + 0xAC); } catch { return 0; } } }
        public int IconUnderlay { get { try { return *(int*)(WeeniePtr + 0xB0); } catch { return 0; } } }
        public int ContainerID { get { try { return *(int*)(WeeniePtr + 0xB4); } catch { return 0; } } }
        public int WielderID { get { try { return *(int*)(WeeniePtr + 0xB8); } catch { return 0; } } }
        public int Priority { get { try { return *(int*)(WeeniePtr + 0xBC); } catch { return 0; } } }
        public INVENTORY_LOC ValidLocations { get { try { return (INVENTORY_LOC)(*(int*)(WeeniePtr + 0xC0)); } catch { return 0; } } }
        public INVENTORY_LOC Location { get { try { return (INVENTORY_LOC)(*(int*)(WeeniePtr + 0xC4)); } catch { return 0; } } }
        public int ItemsCapacity { get { try { return *(int*)(WeeniePtr + 0xC8); } catch { return 0; } } }
        internal int _itemsContained = 0;
        /// <summary>
        /// Allows set, for batch operations- set value with only persist inside of this Weenie reference
        /// </summary>
        public int ItemsContained { get { if (_itemsContained != 0) { return _itemsContained; } try { return *(int*)(*ObjInventory + 0x1C); } catch { return 0; } } set { _itemsContained = value; } }
        public int FreeSpace { get { return ItemsCapacity - ItemsContained; } }
        public int ContainersCapacity { get { try { return *(int*)(WeeniePtr + 0xCC); } catch { return 0; } } }
        public int ContainersContained { get { try { return *(int*)(*ObjInventory + 0x34); } catch { return 0; } } }
        public ITEM_TYPE Type { get { try { return (ITEM_TYPE)(*(int*)(WeeniePtr + 0xD0)); } catch { return 0; } } }
        public int Value { get { try { return *(int*)(WeeniePtr + 0xD4); } catch { return 0; } } }
        public Useable Useability { get { try { return (Useable)(*(int*)(WeeniePtr + 0xD8)); } catch { return 0; } } }
        public int UsesRemaining { get { try { return *(int*)(WeeniePtr + 0xF0); } catch { return 0; } } }
        public int UsesMax { get { try { return *(int*)(WeeniePtr + 0xF4); } catch { return 0; } } }
        internal int _stackCount = 0;
        /// <summary>
        /// Allows set, for batch operations- set value with only persist inside of this Weenie reference
        /// </summary>
        public int StackCount { get { if (_stackCount != 0) { return _stackCount; } try { return *(int*)(WeeniePtr + 0xF8); } catch { return 0; } } set { _stackCount = value; } }
        public int StackMax { get { try { return *(int*)(WeeniePtr + 0xFC); } catch { return 0; } } }
        public Bitfield Bools { get { try { return (Bitfield)(*(int*)(WeeniePtr + 0x100)); } catch { return 0; } } }
        public int Burden { get { try { return *(int*)(WeeniePtr + 0x10C); } catch { return 0; } } }
        public int Monarch { get { try { return *(int*)(WeeniePtr + 0x128); } catch { return 0; } } }
        public int Material { get { try { return *(int*)(WeeniePtr + 0x12C); } catch { return 0; } } }
        public float Workmanship { get { try { return *(float*)(WeeniePtr + 0x130); } catch { return 0f; } } }
        internal ObjectClass? _objectclass = null;
        public ObjectClass ObjectClass { get { if (_objectclass == null) { _objectclass = _ObjectClass(); } return (ObjectClass)_objectclass; } }
        public bool Collidable { get { try { return (*(int*)(PhysicsPtr + 0xA8) & 0x04) == 0; } catch { return false; } } }
        public float Distance { get { try { return *(float*)(PhysicsPtr + 0x20); } catch { return 0f; } } }
        public bool InInventory { get { try { if (ContainerID == *P.CPhysicsPart__player_iid) return true; if (new Weenie(ContainerID).ContainerID == *P.CPhysicsPart__player_iid) return true; return false; } catch { return false; } } }
        public Weenie(int weenie_id) {
            WeeniePtr = Core.GetWeeniePtr(weenie_id);
        }
        public bool Drop() {
            if (!Valid) return false;
            if (Core.GetBusyState != 0) return false;
            *P.ACCWeenieObject__prevRequestObjectID = Id;
            *P.ACCWeenieObject__prevRequest = 6;
            *P.ACCWeenieObject__prevRequestTime = *P.Timer__cur_time;
            return P.Call_CM_Inventory__Event_DropItem(Id);
        }
        public bool GiveTo(Weenie target, int quantity = 0) {
            if (!Valid) return false;
            if (!target.Valid) return false;
            if (Core.GetBusyState != 0) return false;
            if (ContainerID != *P.CPhysicsPart__player_iid) {
                var wc = new Weenie(ContainerID);
                if (!wc.Valid || wc.ContainerID != *P.CPhysicsPart__player_iid) {
                    return false;
                }
            }
            if (quantity == 0) quantity = StackCount;
            else if (quantity > StackCount) quantity = StackCount;
            StackCount -= quantity;
            if (quantity == 0) quantity = 1;
            *P.ACCWeenieObject__prevRequestObjectID = Id;
            *P.ACCWeenieObject__prevRequest = 9;
            *P.ACCWeenieObject__prevRequestTime = *P.Timer__cur_time;
            return P.Call_CM_Inventory__Event_GiveObjectRequest(target.Id, Id, quantity);
        }
        public bool Use() {
            if (!Valid) return false;
            Core.IncrementBusyCount();
            return P.Call_CM_Inventory__Event_UseEvent(Id);
        }
        public bool UseOn(Weenie target) {
            if (!Valid) return false;
            if (!target.Valid) return false;
            Core.IncrementBusyCount();
            return P.Call_CM_Inventory__Event_UseWithTargetEvent(Id, target.Id);
        }
        public bool StackTo(Weenie target, int amount_to_stack = 0) {
            if (!Valid) return false;
            if (!target.Valid) return false;
            if (amount_to_stack == 0) amount_to_stack = StackCount;
            StackCount -= amount_to_stack;
            target.StackCount += amount_to_stack;
            return P.Call_CM_Inventory__Event_StackableMerge(Id, target.Id, amount_to_stack);
        }
        public void MoveTo(int containerid, int position = 0) {
            if (!Valid) return;
            P.Call_CM_Inventory__Event_PutItemInContainer(Id, containerid, position);
        }
        public void Split(int target_container, int position, int amount_to_split) {
            if (!Valid) return;
            if (StackCount == 0) return;
            if (amount_to_split == 0) amount_to_split = 1;
            if (amount_to_split > StackCount) amount_to_split = StackCount - 1;
            StackCount -= amount_to_split;
            P.Call_CM_Inventory__Event_StackableSplitToContainer(Id, target_container, position, amount_to_split);
        }
        public void Delete() {
            if (!Valid) return;
            if (ContainerID != *P.CPhysicsPart__player_iid) {
                var wc = new Weenie(ContainerID);
                if (!wc.Valid || wc.ContainerID != *P.CPhysicsPart__player_iid) {
                    return;
                }
            }
            P.Call_CObjectMaint__DeleteObject(Id);
        }
        private Decal.Adapter.Wrappers.ObjectClass _ObjectClass() {
            Decal.Adapter.Wrappers.ObjectClass objectClass = Decal.Adapter.Wrappers.ObjectClass.Unknown;
            int _type = (int)Type;
            int _bools = (int)Bools;
            // int num3 = 0;
            // if (this.a.ContainsKey(218103832)) num3 = this.a[218103832]; createflags1
            if ((_type & 1) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.MeleeWeapon;
            else if ((_type & 2) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Armor;
            else if ((_type & 4) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Clothing;
            else if ((_type & 8) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Jewelry;
            else if ((_type & 16) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Monster;
            else if ((_type & 32) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Food;
            else if ((_type & 64) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Money;
            else if ((_type & 128) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Misc;
            else if ((_type & 256) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.MissileWeapon;
            else if ((_type & 512) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Container;
            else if ((_type & 1024) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Bundle;
            else if ((_type & 2048) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Gem;
            else if ((_type & 4096) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.SpellComponent;
            else if ((_type & 16384) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Key;
            else if ((_type & 32768) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.WandStaffOrb;
            else if ((_type & 65536) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Portal;
            else if ((_type & 262144) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.TradeNote;
            else if ((_type & 524288) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.ManaStone;
            else if ((_type & 1048576) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Services;
            else if ((_type & 2097152) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Plant;
            else if ((_type & 4194304) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.BaseCooking;
            else if ((_type & 8388608) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.BaseAlchemy;
            else if ((_type & 16777216) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.BaseFletching;
            else if ((_type & 33554432) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.CraftedCooking;
            else if ((_type & 67108864) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.CraftedAlchemy;
            else if ((_type & 134217728) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.CraftedFletching;
            else if ((_type & 536870912) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Ust;
            else if ((_type & 1073741824) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Salvage;
            if ((_bools & 8) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Player;
            else if ((_bools & 512) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Vendor;
            else if ((_bools & 4096) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Door;
            else if ((_bools & 8192) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Corpse;
            else if ((_bools & 16384) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Lifestone;
            else if ((_bools & 32768) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Food;
            else if ((_bools & 65536) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.HealingKit;
            else if ((_bools & 131072) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Lockpick;
            else if ((_bools & 262144) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Portal;
            else if ((_bools & 8388608) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Foci;
            else if ((_bools & 1) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Container;
            if ((_type & 8192) > 0 && (_bools & 256) > 0 && objectClass == Decal.Adapter.Wrappers.ObjectClass.Unknown) {
                if ((_bools & 2) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Journal;
                else if ((_bools & 4) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Sign;
                else if ((_bools & 15) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Book;
            }
            //if ((_type & 8192) > 0 && (num3 & 4194304) > 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Scroll;
            if (objectClass == Decal.Adapter.Wrappers.ObjectClass.Monster && (_bools & 16) == 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Npc;
            if (objectClass == Decal.Adapter.Wrappers.ObjectClass.Monster && (_bools & 67108864) != 0) objectClass = Decal.Adapter.Wrappers.ObjectClass.Npc;
            return objectClass;
        }

        public enum Bitfield {
            BF_OPENABLE = 0x1,
            BF_INSCRIBABLE = 0x2,
            BF_STUCK = 0x4,
            BF_PLAYER = 0x8,
            BF_ATTACKABLE = 0x10,
            BF_PLAYER_KILLER = 0x20,
            BF_HIDDEN_ADMIN = 0x40,
            BF_UI_HIDDEN = 0x80,
            BF_BOOK = 0x100,
            BF_VENDOR = 0x200,
            BF_PKSWITCH = 0x400,
            BF_NPKSWITCH = 0x800,
            BF_DOOR = 0x1000,
            BF_CORPSE = 0x2000,
            BF_LIFESTONE = 0x4000,
            BF_FOOD = 0x8000,
            BF_HEALER = 0x10000,
            BF_LOCKPICK = 0x20000,
            BF_PORTAL = 0x40000,
            BF_ADMIN = 0x100000,
            BF_FREE_PKSTATUS = 0x200000,
            BF_IMMUNE_CELL_RESTRICTIONS = 0x400000,
            BF_REQUIRES_PACKSLOT = 0x800000,
            BF_RETAINED = 0x1000000,
            BF_PKLITE_PKSTATUS = 0x2000000,
            BF_INCLUDES_SECOND_HEADER = 0x4000000,
            BF_BINDSTONE = 0x8000000,
            BF_VOLATILE_RARE = 0x10000000,
            BF_WIELD_ON_USE = 0x20000000,
            BF_WIELD_LEFT = 0x40000000,
        };
        public enum Useable {
            USEABLE_UNDEF = 0x0,
            USEABLE_NO = 0x1,
            USEABLE_SELF = 0x2,
            USEABLE_WIELDED = 0x4,
            USEABLE_CONTAINED = 0x8,
            USEABLE_VIEWED = 0x10,
            USEABLE_REMOTE = 0x20,
            USEABLE_NEVER_WALK = 0x40,
            USEABLE_OBJSELF = 0x80,
            USEABLE_CONTAINED_VIEWED = 0x18,
            USEABLE_CONTAINED_VIEWED_REMOTE = 0x38,
            USEABLE_CONTAINED_VIEWED_REMOTE_NEVER_WALK = 0x78,
            USEABLE_VIEWED_REMOTE = 0x30,
            USEABLE_VIEWED_REMOTE_NEVER_WALK = 0x70,
            USEABLE_REMOTE_NEVER_WALK = 0x60,
            USEABLE_SOURCE_WIELDED_TARGET_WIELDED = 0x40004,
            USEABLE_SOURCE_WIELDED_TARGET_CONTAINED = 0x80004,
            USEABLE_SOURCE_WIELDED_TARGET_VIEWED = 0x100004,
            USEABLE_SOURCE_WIELDED_TARGET_REMOTE = 0x200004,
            USEABLE_SOURCE_WIELDED_TARGET_REMOTE_NEVER_WALK = 0x600004,
            USEABLE_SOURCE_CONTAINED_TARGET_WIELDED = 0x40008,
            USEABLE_SOURCE_CONTAINED_TARGET_CONTAINED = 0x80008,
            USEABLE_SOURCE_CONTAINED_TARGET_OBJSELF_OR_CONTAINED = 0x880008,
            USEABLE_SOURCE_CONTAINED_TARGET_SELF_OR_CONTAINED = 0xA0008,
            USEABLE_SOURCE_CONTAINED_TARGET_VIEWED = 0x100008,
            USEABLE_SOURCE_CONTAINED_TARGET_REMOTE = 0x200008,
            USEABLE_SOURCE_CONTAINED_TARGET_REMOTE_NEVER_WALK = 0x600008,
            USEABLE_SOURCE_CONTAINED_TARGET_REMOTE_OR_SELF = 0x220008,
            USEABLE_SOURCE_VIEWED_TARGET_WIELDED = 0x40010,
            USEABLE_SOURCE_VIEWED_TARGET_CONTAINED = 0x80010,
            USEABLE_SOURCE_VIEWED_TARGET_VIEWED = 0x100010,
            USEABLE_SOURCE_VIEWED_TARGET_REMOTE = 0x200010,
            USEABLE_SOURCE_REMOTE_TARGET_WIELDED = 0x40020,
            USEABLE_SOURCE_REMOTE_TARGET_CONTAINED = 0x80020,
            USEABLE_SOURCE_REMOTE_TARGET_VIEWED = 0x100020,
            USEABLE_SOURCE_REMOTE_TARGET_REMOTE = 0x200020,
            USEABLE_SOURCE_REMOTE_TARGET_REMOTE_NEVER_WALK = 0x600020,
            USEABLE_SOURCE_MASK = 0xFFFF,
            USEABLE_TARGET_MASK = -65536, // 0xFFFF0000,
            FORCE_ITEM_USEABLE_32_BIT = 0x7FFFFFFF,
        };
        public enum INVENTORY_LOC {
            NONE_LOC =              0x00000000,
            HEAD_WEAR_LOC =         0x00000001,
            CHEST_WEAR_LOC =        0x00000002,
            ABDOMEN_WEAR_LOC =      0x00000004,
            UPPER_ARM_WEAR_LOC =    0x00000008,
            LOWER_ARM_WEAR_LOC =    0x00000010,
            HAND_WEAR_LOC =         0x00000020,
            UPPER_LEG_WEAR_LOC =    0x00000040,
            LOWER_LEG_WEAR_LOC =    0x00000080,
            FOOT_WEAR_LOC =         0x00000100,
            CHEST_ARMOR_LOC =       0x00000200,
            ABDOMEN_ARMOR_LOC =     0x00000400,
            UPPER_ARM_ARMOR_LOC =   0x00000800,
            LOWER_ARM_ARMOR_LOC =   0x00001000,
            UPPER_LEG_ARMOR_LOC =   0x00002000,
            LOWER_LEG_ARMOR_LOC =   0x00004000,
            NECK_WEAR_LOC =         0x00008000,
            WRIST_WEAR_LEFT_LOC =   0x00010000,
            WRIST_WEAR_RIGHT_LOC =  0x00020000,
            FINGER_WEAR_LEFT_LOC =  0x00040000,
            FINGER_WEAR_RIGHT_LOC = 0x00080000,
            MELEE_WEAPON_LOC =      0x00100000,
            SHIELD_LOC =            0x00200000,
            MISSILE_WEAPON_LOC =    0x00400000,
            MISSILE_AMMO_LOC =      0x00800000,
            HELD_LOC =              0x01000000,
            TWO_HANDED_LOC =        0x02000000,
            TRINKET_ONE_LOC =       0x04000000,
            CLOAK_LOC =             0x08000000,
            SIGIL_ONE_LOC =         0x10000000,
            SIGIL_TWO_LOC =         0x20000000,
            SIGIL_THREE_LOC =       0x40000000,

            CLOTHING_LOC =          0x080001FF,
            ARMOR_LOC =             0x00007E00,
            JEWELRY_LOC =           0x7C0F8000,
            WRIST_WEAR_LOC =        0x00030000,
            FINGER_WEAR_LOC =       0x000C0000,
            SIGIL_LOC =             0x70000000,
            READY_SLOT_LOC =        0x03F00000,
            WEAPON_LOC =            0x02500000,
            WEAPON_READY_SLOT_LOC = 0x03500000,
            ALL_LOC =               0x7FFFFFFF,
        };
        public enum ITEM_TYPE {
            TYPE_UNDEF = 0x0,
            TYPE_MELEE_WEAPON = 0x1,
            TYPE_ARMOR = 0x2,
            TYPE_CLOTHING = 0x4,
            TYPE_JEWELRY = 0x8,
            TYPE_CREATURE = 0x10,
            TYPE_FOOD = 0x20,
            TYPE_MONEY = 0x40,
            TYPE_MISC = 0x80,
            TYPE_MISSILE_WEAPON = 0x100,
            TYPE_CONTAINER = 0x200,
            TYPE_USELESS = 0x400,
            TYPE_GEM = 0x800,
            TYPE_SPELL_COMPONENTS = 0x1000,
            TYPE_WRITABLE = 0x2000,
            TYPE_KEY = 0x4000,
            TYPE_CASTER = 0x8000,
            TYPE_PORTAL = 0x10000,
            TYPE_LOCKABLE = 0x20000,
            TYPE_PROMISSORY_NOTE = 0x40000,
            TYPE_MANASTONE = 0x80000,
            TYPE_SERVICE = 0x100000,
            TYPE_MAGIC_WIELDABLE = 0x200000,
            TYPE_CRAFT_COOKING_BASE = 0x400000,
            TYPE_CRAFT_ALCHEMY_BASE = 0x800000,
            TYPE_CRAFT_FLETCHING_BASE = 0x1000000,
            TYPE_CRAFT_ALCHEMY_INTERMEDIATE = 0x4000000,
            TYPE_CRAFT_FLETCHING_INTERMEDIATE = 0x8000000,
            TYPE_LIFESTONE = 0x10000000,
            TYPE_TINKERING_TOOL = 0x20000000,
            TYPE_TINKERING_MATERIAL = 0x40000000,
            TYPE_GAMEBOARD = -2147483648, //0x80000000,
            TYPE_PORTAL_MAGIC_TARGET = 0x10010000,
            TYPE_LOCKABLE_MAGIC_TARGET = 0x280,
            TYPE_VESTEMENTS = 0x6,
            TYPE_WEAPON = 0x101,
            TYPE_WEAPON_OR_CASTER = 0x8101,
            TYPE_ITEM = 0x2DFBEF,
            TYPE_REDIRECTABLE_ITEM_ENCHANTMENT_TARGET = 0x8107,
            TYPE_ITEM_ENCHANTABLE_TARGET = 0x88B8F,
            TYPE_SELF = 0x0,
            TYPE_VENDOR_SHOPKEEP = 0x480467A7,
            TYPE_VENDOR_GROCER = 0x446220,
        };
    }
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
}
