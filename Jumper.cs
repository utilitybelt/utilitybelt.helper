﻿using System;
using System.Runtime.InteropServices;

namespace UBHelper {
    /// <summary>
    /// Helper class to assist with jumping
    /// </summary>
    public static unsafe class Jumper {
        internal static bool isJumping = false;
        internal static float JumpTrigger = 1.0f;
        internal static bool holdZ = false;
        internal static bool holdC = false;
        internal static bool holdW = true;
        internal static bool holdX = false;
        internal static bool holdSHIFT = true;
        internal static double JumpStart = 0d;
        internal static float direction = 0f;

        /// <summary>
        /// Tells your character to jump.
        /// </summary>
        /// <param name="power">how much power to put into the jump, between 0-1</param>
        /// <param name="holdSHIFT">set to true to perform a "walk" jump</param>
        /// <param name="holdW">set to true to hold the move forward button while jumping</param>
        /// <param name="holdX">set to true to hold the move backward button while jumping</param>
        /// <param name="holdZ">set to true to hold the left strafe button while jumping</param>
        /// <param name="holdC">set to true to hold the right strafe button while jumping</param>
        public static void Jump(float direction, float power, bool holdSHIFT, bool holdW, bool holdX, bool holdZ, bool holdC) {
            if (!isJumping) {
                isJumping = true;
                Jumper.holdSHIFT = holdSHIFT;
                Jumper.holdW = holdW;
                Jumper.holdX = holdX;
                Jumper.holdZ = holdZ;
                Jumper.holdC = holdC;
                Jumper.JumpTrigger = power;
                Jumper.direction = direction;
                JumpStart = Core.curTime;
                if (JumpTrigger > 1.0f) JumpTrigger = 1.0f;
                P.CM_Character__SendNotice_SetPowerbarLevel.Setup(new P.def_CM_Character__SendNotice_SetPowerbarLevel(Hook_CM_Character__SendNotice_SetPowerbarLevel));
                Core.RadarUpdate += Core_RadarUpdate;
                P.Call_ClientCombatSystem__CommenceJump();
                //Core.W("Pressing \"SPACE\"");
            }
        }

        private static void Core_RadarUpdate(double uptime) {
            if (Core.curTime - JumpStart > 1.25d) {
                JumpStart = Core.curTime;
                P.Call_ClientCombatSystem__CommenceJump();
            }
        }

        /// <summary>
        /// Cancel the current jump attempt
        /// </summary>
        public static void JumpCancel() {
            if (isJumping) {
                isJumping = false;
                Core.RadarUpdate -= Core_RadarUpdate;
                P.CM_Character__SendNotice_SetPowerbarLevel.Remove();
                P.CMotionInterp__LeaveGround.Remove();
                P.CMotionInterp__HitGround.Remove();
            }
        }

        //todo
        /// <summary>
        /// Raised when the jump has been completed, and you hit the ground.
        /// </summary>
        public static event Core.callback JumpComplete;

        /// <summary>
        /// Raised when the jump has been completed, and you hit the ground.
        /// </summary>
        public static event Core.callback JumpStarted;

        internal static char Hook_CM_Character__SendNotice_SetPowerbarLevel(PowerBarMode i_pbm, float i_fLevel) {
            char retval = P.Call_CM_Character__SendNotice_SetPowerbarLevel(i_pbm, i_fLevel);
            if (i_pbm == PowerBarMode.PBM_JUMP && i_fLevel >= JumpTrigger) {
                Core.RadarUpdate -= Core_RadarUpdate;
                P.CM_Character__SendNotice_SetPowerbarLevel.Remove();

                //Core.W("Released \"SPACE\"");
                double max_speed = P.Call_CMotionInterp__get_max_speed() / 4d;

                if (holdW || holdX) {
                    if (holdW) {
                        *(uint*)(Core.CMotionInterp + 0x004C) = 0x44000007;
                        if (holdSHIFT)
                            *(float*)(Core.CMotionInterp + 0x0050) = 1.0f;
                        else
                            *(float*)(Core.CMotionInterp + 0x0050) = (float)(max_speed);
                    } else {
                        *(uint*)(Core.CMotionInterp + 0x004C) = 0x45000005;
                        if (holdSHIFT)
                            *(float*)(Core.CMotionInterp + 0x0050) = -1f;
                        else
                            *(float*)(Core.CMotionInterp + 0x0050) = (float)(-0.64999998d * max_speed);
                    }
                } else {
                    *(uint*)(Core.CMotionInterp + 0x004C) = 0x41000003;
                }
                if (holdZ || holdC) {
                    *(uint*)(Core.CMotionInterp + 0x0054) = 0x6500000F;
                    double max_sidestep_speed = max_speed * 1.24799996d;
                    if (holdSHIFT) max_sidestep_speed *= 0.5d;
                    if (max_sidestep_speed > 3d) max_sidestep_speed = 3d;
                    if (holdC) {
                        *(float*)(Core.CMotionInterp + 0x0058) = (float)(max_sidestep_speed);
                    } else {
                        *(float*)(Core.CMotionInterp + 0x0058) = (float)(-1 * max_sidestep_speed);
                    }
                } else {
                    *(uint*)(Core.CMotionInterp + 0x0054) = 0x00000000;
                }
                *(uint*)(Core.CMotionInterp + 0x005C) = 0x00000000;
                P.CMotionInterp__LeaveGround.Setup(new P.def_CMotionInterp__LeaveGround(Hook_CMotionInterp__LeaveGround));
                P.Call_CPhysicsObj__set_heading(*P.CPhysicsPart__player_object, direction, 0);
                P.Call_ClientCombatSystem__DoJump(1);

                if (holdZ || holdC) {
                    *(uint*)(Core.CMotionInterp + 0x0054) = 0x00000000;
                }
                if (holdW || holdX) {
                    *(uint*)(Core.CMotionInterp + 0x004C) = 0x41000003;
                }
            }
            return retval;
        }

        internal static void Hook_CMotionInterp__LeaveGround(int CMotionInterp) {
            P.Call_CMotionInterp__LeaveGround(CMotionInterp);
            int targetphy = 0;
            try { targetphy = *(int*)(CMotionInterp + 0x0008); } catch { return; }
            if (targetphy == Core.CPhysics) {
                //Core.W($"You left the ground!");
                P.CMotionInterp__LeaveGround.Remove();
                P.CMotionInterp__HitGround.Setup(new P.def_CMotionInterp__HitGround(Hook_CMotionInterp__HitGround));
                JumpStarted?.Invoke();
            }
        }

        internal static void Hook_CMotionInterp__HitGround(int CMotionInterp) {
            P.Call_CMotionInterp__HitGround(CMotionInterp);
            int targetphy = 0;

            try { targetphy = *(int*)(CMotionInterp + 0x0008); } catch { return; }
            if (targetphy == Core.CPhysics) {
                //Core.W($"You hit the ground!");
                P.CMotionInterp__HitGround.Remove();
                isJumping = false;
                JumpComplete?.Invoke();
            }
        }


#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
        public enum PowerBarMode {
            PBM_UNDEF = 0x0,
            PBM_COMBAT = 0x1,
            PBM_ADVANCED_COMBAT = 0x2,
            PBM_JUMP = 0x3,
            PBM_DDD = 0x4,
        };
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
