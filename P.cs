﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;


namespace UBHelper {
    internal unsafe static class P {
        internal static readonly UIntPtr doublesize = new UIntPtr(sizeof(double));
        internal static readonly UIntPtr longsize = new UIntPtr(sizeof(long));
        internal static readonly UIntPtr intsize = new UIntPtr(sizeof(int));
        internal static readonly UIntPtr floatsize = new UIntPtr(sizeof(float));
        internal static readonly UIntPtr shortsize = new UIntPtr(sizeof(short));
        internal static readonly UIntPtr bytesize = new UIntPtr(sizeof(byte));

        internal static readonly IntPtr ResolutionPatchWidth = (IntPtr)0x00439381;
        internal static readonly IntPtr ResolutionPatchHeight = (IntPtr)0x0043938B;

        internal static int Call_CBaseQualities__InqInt(int CBaseQualities, int stype, int* retval, int raw, int allow_negative) => ((def_CBaseQualities__InqInt)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590C20, typeof(def_CBaseQualities__InqInt)))(CBaseQualities, stype, retval, raw, allow_negative);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqInt(int CBaseQualities, int stype, int* retval, int raw, int allow_negative); // int __thiscall CBaseQualities::InqInt(CBaseQualities *this, unsigned int stype, int *retval, int raw, int allow_negative)
        internal static int Call_CBaseQualities__InqInt64(int CBaseQualities, int stype, long* retval) => ((def_CBaseQualities__InqInt64)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590C70, typeof(def_CBaseQualities__InqInt64)))(CBaseQualities, stype, retval);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqInt64(int CBaseQualities, int stype, long* retval); // int __thiscall CBaseQualities::InqInt64(CBaseQualities *this, unsigned int stype, __int64 *retval)
        internal static int Call_CBaseQualities__InqBool(int CBaseQualities, int stype, bool* retval) => ((def_CBaseQualities__InqBool)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590CA0, typeof(def_CBaseQualities__InqBool)))(CBaseQualities, stype, retval);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqBool(int CBaseQualities, int stype, bool* retval); // int __thiscall CBaseQualities::InqBool(CBaseQualities *this, unsigned int stype, int *retval)

        internal static int Call_CBaseQualities__InqFloat(int CBaseQualities, int stype, double* retval, int raw) => ((def_CBaseQualities__InqFloat)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590CD0, typeof(def_CBaseQualities__InqFloat)))(CBaseQualities, stype, retval, raw);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqFloat(int CBaseQualities, int stype, double* retval, int raw); // int __thiscall CBaseQualities::InqFloat(CBaseQualities *this, unsigned int stype, long double *retval, int raw)

        internal static int Call_CBaseQualities__InqDataID(int CBaseQualities, int stype, uint* retval) => ((def_CBaseQualities__InqDataID)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590D20, typeof(def_CBaseQualities__InqDataID)))(CBaseQualities, stype, retval);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqDataID(int CBaseQualities, int stype, uint* retval); // int __thiscall CBaseQualities::InqDataID(CBaseQualities *this, unsigned int stype, IDClass<_tagDataID,32,0> *retval)

        internal static int Call_CBaseQualities__InqInstanceID(int CBaseQualities, int stype, uint* retval) => ((def_CBaseQualities__InqInstanceID)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590D50, typeof(def_CBaseQualities__InqInstanceID)))(CBaseQualities, stype, retval);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqInstanceID(int CBaseQualities, int stype, uint* retval); // int __thiscall CBaseQualities::InqInstanceID(CBaseQualities *this, unsigned int stype, unsigned int *retval)

        internal static int* Call_ACCWeenieObject__GetObjectName(int ACCWeenieObject, NameType _nameType) => ((def_ACCWeenieObject__GetObjectName)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0058F510, typeof(def_ACCWeenieObject__GetObjectName)))(ACCWeenieObject, _nameType, 0);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int* def_ACCWeenieObject__GetObjectName(int ACCWeenieObject, NameType _nameType, int _playerIsBackpack); //int *__thiscall ACCWeenieObject::GetObjectName(ACCWeenieObject *this, NameType _nameType, int _playerIsBackpack)

        internal static bool Call_Proto_UI__LogOffCharacter(int gid) => ((def_Proto_UI__LogOffCharacter)Marshal.GetDelegateForFunctionPointer(Proto_UI__LogOffCharacter.Entrypoint, typeof(def_Proto_UI__LogOffCharacter)))(gid);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_Proto_UI__LogOffCharacter(int gid); // bool __cdecl Proto_UI::LogOffCharacter(unsigned int gid)
        internal static Hooker Proto_UI__LogOffCharacter = new Hooker(0x005475E0, 0x00563BBF);

        internal static void Call_UIFlow__UseNewMode(int UIFlow) => ((def_UIFlow__UseNewMode)Marshal.GetDelegateForFunctionPointer(UIFlow__UseNewMode.Entrypoint, typeof(def_UIFlow__UseNewMode)))(UIFlow);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_UIFlow__UseNewMode(int UIFlow); // void __thiscall UIFlow::UseNewMode(UIFlow *this)
        internal static Hooker UIFlow__UseNewMode = new Hooker(0x00479AA0, 0x00479BB4);

        internal static void Call_Device__DoFrameSleep() => ((def_Device__DoFrameSleep)Marshal.GetDelegateForFunctionPointer(Device__DoFrameSleep.Entrypoint, typeof(def_Device__DoFrameSleep)))();
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate void def_Device__DoFrameSleep(); // void __cdecl Device::DoFrameSleep()
        internal static Hooker Device__DoFrameSleep = new Hooker(0x004392B0, 0x00412015);

        internal static void Call_Device_Activate() => ((def_Device__Deactivate)Marshal.GetDelegateForFunctionPointer((IntPtr)0x004392F0, typeof(def_Device__Deactivate)))();
        internal static void Call_Device__Deactivate() => ((def_Device__Deactivate)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00439320, typeof(def_Device__Deactivate)))();
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate void def_Device__Deactivate(); // void __cdecl Device::Deactivate() 

        internal static void Call_SmartBox__SetNormalMode(int SmartBox) => ((def_SmartBox__SetNormalMode)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00453160, typeof(def_SmartBox__SetNormalMode)))(SmartBox);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_SmartBox__SetNormalMode(int SmartBox); //int __thiscall SmartBox::SetNormalMode(SmartBox*this)

        internal static void Call_SmartBox__update_viewer(int SmartBox) => ((def_SmartBox__update_viewer)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00453D80, typeof(def_SmartBox__update_viewer)))(SmartBox);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_SmartBox__update_viewer(int SmartBox);// void __thiscall SmartBox::update_viewer(SmartBox *this)

        internal static void Call_SmartBox__Draw(int SmartBox) => ((def_SmartBox__Draw)Marshal.GetDelegateForFunctionPointer(SmartBox__Draw.Entrypoint, typeof(def_SmartBox__Draw)))(SmartBox);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_SmartBox__Draw(int SmartBox); // ???? not in source
        internal static Hooker SmartBox__Draw = new Hooker(0x00455610, 0x00412006);

        internal static int Call_UIElement__GetChildRecursive(int UIElement, int _ID) => ((def_UIElement__GetChildRecursive)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00463C00, typeof(def_UIElement__GetChildRecursive)))(UIElement, _ID);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_UIElement__GetChildRecursive(int UIElement, int _ID); // UIElement *__thiscall UIElement::GetChildRecursive(UIElement *this, unsigned int _ID)

        internal static void Call_UIElement__MoveTo(int UIElement, int x, int y) => ((def_UIElement__MoveTo)Marshal.GetDelegateForFunctionPointer((IntPtr)0x004634C0, typeof(def_UIElement__MoveTo)))(UIElement, x, y);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_UIElement__MoveTo(int UIElement, int x, int y); // void __thiscall UIElement::MoveTo(UIElement*this, const int _x, const int _y)

        internal static void Call_UIElement__ResizeTo(int UIElement, int width, int height) => ((def_UIElement__ResizeTo)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00463D60, typeof(def_UIElement__ResizeTo)))(UIElement, width, height);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_UIElement__ResizeTo(int UIElement, int width, int height); // void __userpurge UIElement::ResizeTo(UIElement *this@<ecx>, int a2@<edi>, const int _width, const int _height)

        internal static int Call_UIRegion__GetX(int UIRegion) => ((def_UIRegion_Get)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0069FE00, typeof(def_UIRegion_Get)))(UIRegion); // unsigned int __thiscall UIRegion::GetScreenX0(UIRegion *this)    
        internal static int Call_UIRegion__GetY(int UIRegion) => ((def_UIRegion_Get)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0069FE30, typeof(def_UIRegion_Get)))(UIRegion); // unsigned int __thiscall UIRegion::GetScreenY0(UIRegion *this)    
        internal static int Call_UIRegion__GetWidth(int UIRegion) => ((def_UIRegion_Get)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0069FE60, typeof(def_UIRegion_Get)))(UIRegion); // int __thiscall UIRegion::GetWidth(UIRegion *this)                
        internal static int Call_UIRegion__GetHeight(int UIRegion) => ((def_UIRegion_Get)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0069FE70, typeof(def_UIRegion_Get)))(UIRegion); // int __thiscall UIRegion::GetHeight(UIRegion *this)               
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_UIRegion_Get(int UIRegion);


        internal static char Call_CM_UI__SendNotice_PlayerDescReceived(int CACQualities, int CPlayerModule) => ((def_CM_UI__SendNotice_PlayerDescReceived)Marshal.GetDelegateForFunctionPointer(CM_UI__SendNotice_PlayerDescReceived.Entrypoint, typeof(def_CM_UI__SendNotice_PlayerDescReceived)))(CACQualities, CPlayerModule);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate char def_CM_UI__SendNotice_PlayerDescReceived(int CACQualities, int CPlayerModule); // char __cdecl CM_UI::SendNotice_PlayerDescReceived(CACQualities *i_playerDesc, CPlayerModule *i_playerModule)
        internal static Hooker CM_UI__SendNotice_PlayerDescReceived = new Hooker(0x0047A200, 0x00564606);

        internal static void Call_CPlayerSystem__SetLogOffStarted(int CPlayerSystem) => ((def_CPlayerSystem__SetLogOffStarted)Marshal.GetDelegateForFunctionPointer(CPlayerSystem__SetLogOffStarted.Entrypoint, typeof(def_CPlayerSystem__SetLogOffStarted)))(CPlayerSystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CPlayerSystem__SetLogOffStarted(int CPlayerSystem); // void __thiscall CPlayerSystem::SetLogOffStarted(CPlayerSystem *this)
        internal static Hooker CPlayerSystem__SetLogOffStarted = new Hooker(0x0055E4F0, 0x004D7AF3);

        internal static void Call_AC1Legacy__PStringBase_char__PStringBase_char(int s_NullBuffer, string message) => ((def_AC1Legacy__PStringBase_char__PStringBase_char)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0048C3E0, typeof(def_AC1Legacy__PStringBase_char__PStringBase_char)))(s_NullBuffer, message);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_AC1Legacy__PStringBase_char__PStringBase_char(int s_NullBuffer, [MarshalAs(UnmanagedType.LPStr)] string message); // void __thiscall AC1Legacy::PStringBase<char>::PStringBase<char>(AC1Legacy::PStringBase<char> *this, const char *str)

        internal static int Call_CObjectMaint__GetObjectA(int object_id) => ((def_CObjectMaint__GetObjectA)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00508890, typeof(def_CObjectMaint__GetObjectA)))(*CObjectMaint, object_id);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CObjectMaint__GetObjectA(int CObjectMaint, int object_id); // HashBaseData<unsigned long> *__thiscall CObjectMaint::GetObjectA(CObjectMaint *this, unsigned int object_id)

        internal static int Call_CObjectMaint__GetWeenieObject(int object_id) => ((def_CObjectMaint__GetWeenieObject)Marshal.GetDelegateForFunctionPointer((IntPtr)0x005088E0, typeof(def_CObjectMaint__GetWeenieObject)))(*CObjectMaint, object_id);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CObjectMaint__GetWeenieObject(int CObjectMaint, int object_id); // HashBaseData<unsigned long> *__thiscall CObjectMaint::GetWeenieObject(CObjectMaint *this, unsigned int object_id)

        internal static IntPtr Entrypoint_CObjectMaint__GetObjectInventory = (IntPtr)0x00508930;
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CObjectMaint__GetObjectInventory(int CObjectMaint, int object_id); // HashBaseData<unsigned long> *__thiscall CObjectMaint::GetObjectInventory(CObjectMaint *this, unsigned int object_id)

        internal static void Call_CObjectMaint__UpdateVisibleObjectList(int CObjectMaint) => ((def_CObjectMaint__UpdateVisibleObjectList)Marshal.GetDelegateForFunctionPointer(CObjectMaint__UpdateVisibleObjectList.Entrypoint, typeof(def_CObjectMaint__UpdateVisibleObjectList)))(CObjectMaint);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CObjectMaint__UpdateVisibleObjectList(int CObjectMaint); // void __thiscall CObjectMaint::UpdateVisibleObjectList(CObjectMaint *this);
        internal static Hooker CObjectMaint__UpdateVisibleObjectList = new Hooker(0x00508E60, 0x005094B7);

        internal static int Call_CObjectMaint__DeleteObject(int object_id) => ((def_CObjectMaint__DeleteObject)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00508FA0, typeof(def_CObjectMaint__DeleteObject)))(*P.CObjectMaint, object_id);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CObjectMaint__DeleteObject(int CObjectMaint, int object_id); // int __thiscall CObjectMaint::DeleteObject(CObjectMaint *this, unsigned int object_id)

        internal static bool Call_CPhysicsObj__set_heading(int CPhysicsObj, float degrees, int send_event) => ((def_CPhysicsObj__set_heading)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00514C60, typeof(def_CPhysicsObj__set_heading)))(CPhysicsObj, degrees, send_event);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate bool def_CPhysicsObj__set_heading(int CPhysicsObj, float degrees, int send_event); //void __thiscall CPhysicsObj::set_heading(CPhysicsObj *this, float degrees, int send_event)

        internal static void Call_CMotionInterp__HitGround(int CMotionInterp) => ((def_CMotionInterp__HitGround)Marshal.GetDelegateForFunctionPointer(CMotionInterp__HitGround.Entrypoint, typeof(def_CMotionInterp__HitGround)))(CMotionInterp);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CMotionInterp__HitGround(int CMotionInterp); // void __thiscall CMotionInterp::HitGround(CMotionInterp *this)
        internal static Hooker CMotionInterp__HitGround = new Hooker(0x005296D0, 0x00524F09);

        internal static void Call_CMotionInterp__LeaveGround(int CMotionInterp) => ((def_CMotionInterp__LeaveGround)Marshal.GetDelegateForFunctionPointer(CMotionInterp__LeaveGround.Entrypoint, typeof(def_CMotionInterp__LeaveGround)))(CMotionInterp);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CMotionInterp__LeaveGround(int CMotionInterp); // void __thiscall CMotionInterp::LeaveGround(CMotionInterp *this)
        internal static Hooker CMotionInterp__LeaveGround = new Hooker(0x00529710, 0x00524F29);

        internal static double Call_CMotionInterp__get_max_speed() => ((def_CMotionInterp__get_max_speed)Marshal.GetDelegateForFunctionPointer((IntPtr)0x005288C0, typeof(def_CMotionInterp__get_max_speed)))(Core.CMotionInterp);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate double def_CMotionInterp__get_max_speed(int CMotionInterp); // double __thiscall CMotionInterp::get_max_speed(CMotionInterp *this)

        internal static void Call_CPlayerSystem__Handle_Login__CharacterSet(int CPlayerSystem, int buff, uint size) => ((def_CPlayerSystem__Handle_Login__CharacterSet)Marshal.GetDelegateForFunctionPointer(CPlayerSystem__Handle_Login__CharacterSet.Entrypoint, typeof(def_CPlayerSystem__Handle_Login__CharacterSet)))(CPlayerSystem, buff, size);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CPlayerSystem__Handle_Login__CharacterSet(int CPlayerSystem, int buff, uint size); // void __thiscall CPlayerSystem::Handle_Login__CharacterSet(CPlayerSystem *this, void *buff, unsigned int size)
        internal static Hooker CPlayerSystem__Handle_Login__CharacterSet = new Hooker(0x00560440, 0x0055D68A);

        internal static int Call_ClientUISystem__Handle_Character__ConfirmationRequest(ConfirmationType confirm, int context, int userData) => ((def_ClientUISystem__Handle_Character__ConfirmationRequest)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00564E40, typeof(def_ClientUISystem__Handle_Character__ConfirmationRequest)))(confirm, context, userData);
        [UnmanagedFunctionPointer(CallingConvention.StdCall)] internal delegate int def_ClientUISystem__Handle_Character__ConfirmationRequest(ConfirmationType confirm, int context, int userData); // unsigned int __stdcall ClientUISystem::Handle_Character__ConfirmationRequest(int confirm, unsigned int context, AC1Legacy::PStringBase<char> *userData)
        internal static Hooker ClientUISystem__Handle_Character__ConfirmationRequest = new Hooker(0x00564E40, 0x006A3C25);

        internal static void Call_ClientUISystem_IncrementBusyCount() => ((def_ClientUISystem_IncrementBusyCount) Marshal.GetDelegateForFunctionPointer((IntPtr)0x00565610, typeof(def_ClientUISystem_IncrementBusyCount)))(*ClientUISystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientUISystem_IncrementBusyCount(int ClientUISystem); // void __thiscall ClientUISystem::IncrementBusyCount(ClientUISystem *this)

        internal static void Call_ClientUISystem_DecrementBusyCount() => ((def_ClientUISystem_DecrementBusyCount)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00565630, typeof(def_ClientUISystem_DecrementBusyCount)))(*ClientUISystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientUISystem_DecrementBusyCount(int ClientUISystem); // void __thiscall ClientUISystem::DecrementBusyCount(ClientUISystem *this)

        internal static void Call_ClientCombatSystem__CommenceJump() => ((def_ClientCombatSystem__CommenceJump)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056BCD0, typeof(def_ClientCombatSystem__CommenceJump)))(*ClientCombatSystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__CommenceJump(int ClientCombatSystem); // void __thiscall ClientCombatSystem::CommenceJump(ClientCombatSystem*this)

        internal static void Call_ClientCombatSystem__DoJump(int autonomous) => ((def_ClientCombatSystem__DoJump)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056BE50, typeof(def_ClientCombatSystem__DoJump)))(*ClientCombatSystem, autonomous);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__DoJump(int ClientCombatSystem, int autonomous); // void __thiscall ClientCombatSystem::DoJump(ClientCombatSystem *this, bool autonomous)

        internal static void Call_CPlayerModule__SaveToServer(bool i_bForceUpdate)  => ((def_CPlayerModule__SaveToServer)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0059B670, typeof(def_CPlayerModule__SaveToServer)))(*CPlayerModule + 0x30, i_bForceUpdate);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CPlayerModule__SaveToServer(int CPlayerModule, bool i_bForceUpdate); // void __thiscall CPlayerModule::SaveToServer(CPlayerModule *this, bool i_bForceUpdate)

        internal static bool Call_PlayerModule__GetOption(int i_eOption) => ((def_PlayerModule__GetOption)Marshal.GetDelegateForFunctionPointer((IntPtr)0x005D4AF0, typeof(def_PlayerModule__GetOption)))((*CPlayerModule) + 0x34, i_eOption);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate bool def_PlayerModule__GetOption(int playermodule, int i_eOption); // bool __thiscall PlayerModule::GetOption(PlayerModule *this, PlayerOption po)

        internal static void Call_PlayerModule__SetOption(int option, bool on) => ((def_PlayerModule__SetOption)Marshal.GetDelegateForFunctionPointer((IntPtr)0x005D4F20, typeof(def_PlayerModule__SetOption)))((*P.CPlayerModule) + 0x34, option, on);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_PlayerModule__SetOption(int playermodule, int i_eOption, bool on); // void __thiscall PlayerModule::SetOption(PlayerModule *this, PlayerOption po, bool on)

        internal static bool Call_CM_Character__Event_ConfirmationResponse(ConfirmationType i_confirmType, int i_context, int i_bAccepted) => ((def_CM_Character__Event_ConfirmationResponse)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A2030, typeof(def_CM_Character__Event_ConfirmationResponse)))(i_confirmType, i_context, i_bAccepted);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_ConfirmationResponse(ConfirmationType i_confirmType, int i_context, int i_bAccepted); // bool __cdecl CM_Character::Event_ConfirmationResponse(int i_confirmType, unsigned int i_context, int i_bAccepted)

        internal static bool Call_CM_Character__Event_LoginCompleteNotification() => ((def_CM_Character__Event_LoginCompleteNotification)Marshal.GetDelegateForFunctionPointer(CM_Character__Event_LoginCompleteNotification.Entrypoint, typeof(def_CM_Character__Event_LoginCompleteNotification)))();
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_LoginCompleteNotification(); // bool __cdecl CM_Character::Event_LoginCompleteNotification()
        internal static Hooker CM_Character__Event_LoginCompleteNotification = new Hooker(0x006A22A0, 0x00563C54);

        internal static char Call_CM_Character__SendNotice_SetPowerbarLevel(Jumper.PowerBarMode i_pbm, float i_fLevel) => ((def_CM_Character__SendNotice_SetPowerbarLevel)Marshal.GetDelegateForFunctionPointer(CM_Character__SendNotice_SetPowerbarLevel.Entrypoint, typeof(def_CM_Character__SendNotice_SetPowerbarLevel)))(i_pbm, i_fLevel);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate char def_CM_Character__SendNotice_SetPowerbarLevel(Jumper.PowerBarMode i_pbm, float i_fLevel); // char __cdecl CM_Character::SendNotice_SetPowerbarLevel(PowerBarMode i_pbm, float i_fLevel)
        internal static Hooker CM_Character__SendNotice_SetPowerbarLevel = new Hooker(0x006A36D0, 0x0056B5DC);

        internal static bool Call_CM_Communication__Event_TalkDirect(int i_msg, int i_target_id) => ((def_CM_Communication__Event_TalkDirect) Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A54A0, typeof(def_CM_Communication__Event_TalkDirect)))(i_msg, i_target_id);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Communication__Event_TalkDirect(int i_msg, int i_target_id); // bool __cdecl CM_Communication::Event_TalkDirect(AC1Legacy::PStringBase<char> *i_msg, unsigned int i_target_id)

        internal static bool Call_CM_Fellowship__Event_AssignNewLeader(int i_target) => ((def_CM_Fellowship__Event_AssignNewLeader)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A6ED0, typeof(def_CM_Fellowship__Event_AssignNewLeader)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_AssignNewLeader(int i_target); // bool __cdecl CM_Fellowship::Event_AssignNewLeader(unsigned int i_target)

        internal static bool Call_CM_Fellowship__Event_ChangeFellowOpeness(int i_open) => ((def_CM_Fellowship__Event_ChangeFellowOpeness)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A6FA0, typeof(def_CM_Fellowship__Event_ChangeFellowOpeness)))(i_open);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_ChangeFellowOpeness(int i_open); // bool __cdecl CM_Fellowship::Event_ChangeFellowOpeness(int i_open)

        internal static bool Call_CM_Fellowship__Event_Dismiss(int i_target) => ((def_CM_Fellowship__Event_Dismiss)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A7070, typeof(def_CM_Fellowship__Event_Dismiss)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_Dismiss(int i_target); // bool __cdecl CM_Fellowship::Event_Dismiss(unsigned int i_target)

        internal static bool Call_CM_Fellowship__Event_Quit(int i_disband) => ((def_CM_Fellowship__Event_Quit)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A7140, typeof(def_CM_Fellowship__Event_Quit)))(i_disband);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_Quit(int i_disband); // bool __cdecl CM_Fellowship::Event_Quit(int i_disband)

        internal static bool Call_CM_Fellowship__Event_Recruit(int i_target) => ((def_CM_Fellowship__Event_Recruit)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A7210, typeof(def_CM_Fellowship__Event_Recruit)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_Recruit(int i_target); // bool __cdecl CM_Fellowship::Event_Recruit(unsigned int i_target)

        internal static bool Call_CM_Fellowship__Event_UpdateRequest(int i_on) => ((def_CM_Fellowship__Event_UpdateRequest)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A72E0, typeof(def_CM_Fellowship__Event_UpdateRequest)))(i_on);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_UpdateRequest(int i_on); // bool __cdecl CM_Fellowship::Event_UpdateRequest(int i_on)

        internal static bool Call_CM_Fellowship__Event_Create(int i_name, int i_share_xp) => ((def_CM_Fellowship__Event_Create)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A7700, typeof(def_CM_Fellowship__Event_Create)))(i_name, i_share_xp);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_Create(int i_name, int i_share_xp); // bool __cdecl CM_Fellowship::Event_Create(AC1Legacy::PStringBase<char> *i_name, int i_share_xp)

        internal static bool Call_CM_Allegiance__Event_BreakAllegiance(int i_target) => ((def_CM_Allegiance__Event_BreakAllegiance)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A78E0, typeof(def_CM_Allegiance__Event_BreakAllegiance)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Allegiance__Event_BreakAllegiance(int i_target); // bool __cdecl CM_Allegiance::Event_BreakAllegiance(unsigned int i_target)

        internal static bool Call_CM_Allegiance__Event_SwearAllegiance(int i_target) => ((def_CM_Allegiance__Event_SwearAllegiance)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A80F0, typeof(def_CM_Allegiance__Event_SwearAllegiance)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Allegiance__Event_SwearAllegiance(int i_target); // bool __cdecl CM_Allegiance::Event_SwearAllegiance(unsigned int i_target)

        internal static char Call_CM_Vendor__SendNotice_CloseVendor(int i_bUpdating) => ((def_CM_Vendor__SendNotice_CloseVendor)Marshal.GetDelegateForFunctionPointer(CM_Vendor__SendNotice_CloseVendor.Entrypoint, typeof(def_CM_Vendor__SendNotice_CloseVendor)))(i_bUpdating);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate char def_CM_Vendor__SendNotice_CloseVendor(int i_bUpdating); // char __cdecl CM_Vendor::SendNotice_CloseVendor(int i_bUpdating)
        internal static Hooker CM_Vendor__SendNotice_CloseVendor = new Hooker(0x006AB1D0, 0x00564E27);

        internal static char Call_CM_Vendor__SendNotice_OpenVendor(int i_vendorID, int i_vendorProfile, int i_itemProfileList, int i_startMode) => ((def_CM_Vendor__SendNotice_OpenVendor)Marshal.GetDelegateForFunctionPointer(CM_Vendor__SendNotice_OpenVendor.Entrypoint, typeof(def_CM_Vendor__SendNotice_OpenVendor)))(i_vendorID, i_vendorProfile, i_itemProfileList, i_startMode);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate char def_CM_Vendor__SendNotice_OpenVendor(int i_vendorID, int i_vendorProfile, int i_itemProfileList, int i_startMode); // char __cdecl CM_Vendor::SendNotice_OpenVendor(unsigned int i_vendorID, VendorProfile *i_vendorProfile, PackableList<ItemProfile> *i_itemProfileList, ShopMode i_startMode)
        internal static Hooker CM_Vendor__SendNotice_OpenVendor = new Hooker(0x006AB220, 0x00566B3C);

        internal static bool Call_CM_Inventory__Event_DropItem(int i_item) => ((def_CM_Inventory__Event_DropItem) Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AC880, typeof(def_CM_Inventory__Event_DropItem)))(i_item);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_DropItem(int i_item); // bool __cdecl CM_Inventory::Event_DropItem(unsigned int i_item)

        internal static bool Call_CM_Inventory__Event_GiveObjectRequest(int i_targetID, int i_objectID, int i_amount) => ((def_CM_Inventory__Event_GiveObjectRequest)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006ACA60, typeof(def_CM_Inventory__Event_GiveObjectRequest)))(i_targetID, i_objectID, i_amount);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_GiveObjectRequest(int i_targetID, int i_objectID, int i_amount); // bool __cdecl CM_Inventory::Event_GiveObjectRequest(unsigned int i_targetID, unsigned int i_objectID, unsigned int i_amount)

        internal static bool Call_CM_Inventory__Event_PutItemInContainer(int i_item, int i_container, int i_loc) => ((def_CM_Inventory__Event_PutItemInContainer)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006ACC80, typeof(def_CM_Inventory__Event_PutItemInContainer)))(i_item, i_container, i_loc);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_PutItemInContainer(int i_item, int i_container, int i_loc); // bool __cdecl CM_Inventory::Event_PutItemInContainer(unsigned int i_item, unsigned int i_container, unsigned int i_loc)

        internal static bool Call_CM_Inventory__Event_StackableMerge(int i_mergeFromID, int i_mergeToID, int i_amount) => ((def_CM_Inventory__Event_StackableMerge)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006ACDD0, typeof(def_CM_Inventory__Event_StackableMerge)))(i_mergeFromID, i_mergeToID, i_amount);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_StackableMerge(int i_mergeFromID, int i_mergeToID, int i_amount); // bool __cdecl CM_Inventory::Event_StackableMerge(unsigned int i_mergeFromID, unsigned int i_mergeToID, int i_amount)

        internal static bool Call_CM_Inventory__Event_StackableSplitToContainer(int i_stackID, int i_containerID, int i_place, int i_amount) => ((def_CM_Inventory__Event_StackableSplitToContainer)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AD030, typeof(def_CM_Inventory__Event_StackableSplitToContainer)))(i_stackID, i_containerID, i_place, i_amount);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_StackableSplitToContainer(int i_stackID, int i_containerID, int i_place, int i_amount); // bool __cdecl CM_Inventory::Event_StackableSplitToContainer(unsigned int i_stackID, unsigned int i_containerID, int i_place, int i_amount)

        internal static bool Call_CM_Inventory__Event_UseEvent(int i_object) => ((def_CM_Inventory__Event_UseEvent)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AD310, typeof(def_CM_Inventory__Event_UseEvent)))(i_object);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_UseEvent(int i_object); // bool __cdecl CM_Inventory::Event_UseEvent(unsigned int i_object)

        internal static bool Call_CM_Inventory__Event_UseWithTargetEvent(int i_object, int i_target) => ((def_CM_Inventory__Event_UseWithTargetEvent)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AD3E0, typeof(def_CM_Inventory__Event_UseWithTargetEvent)))(i_object, i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_UseWithTargetEvent(int i_object, int i_target); // bool __cdecl CM_Inventory::Event_UseWithTargetEvent(unsigned int i_object, unsigned int i_target)

        internal static int Call_CommandInterpreter__TurnToHeading(float new_heading, int run) => ((def_CommandInterpreter__TurnToHeading)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006B54B0, typeof(def_CommandInterpreter__TurnToHeading)))(Core.CommandInterpreter, new_heading, run);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CommandInterpreter__TurnToHeading(int CommandInterpreter, float new_heading, int run); // int __thiscall CommandInterpreter::TurnToHeading(CommandInterpreter *this, float new_heading, int run)

        internal static int Call_Proto_UI__SendEnterWorld(int gid, int account) => ((def_Proto_UI__SendEnterWorld)Marshal.GetDelegateForFunctionPointer(Proto_UI__SendEnterWorld.Entrypoint, typeof(def_Proto_UI__SendEnterWorld)))(gid, account);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate int def_Proto_UI__SendEnterWorld(int gid, int account); // int __cdecl Proto_UI::SendEnterWorld(unsigned int gid, accountID account)
        internal static Hooker Proto_UI__SendEnterWorld = new Hooker(0x00547780, 0x0056069F);

        [UnmanagedFunctionPointer(CallingConvention.StdCall)] internal delegate int extern_sendto(int s, int buf, int len, int flags, int to, int tolen); // int __stdcall sendto(SOCKET s, const char *buf, int len, int flags, const struct sockaddr *to, int tolen)
        internal static Delegate Del_extern_sendto;
        internal static extern_sendto Rtrn_extern_sendto;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)] internal delegate int extern_recvfrom(int s, int buf, int len, int flags, int from, int fromlen); // int __stdcall recvfrom(SOCKET s, char *buf, int len, int flags, struct sockaddr *from, int *fromlen)
        internal static Delegate Del_extern_recvfrom;
        internal static extern_recvfrom Rtrn_extern_recvfrom;

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void __vecDelDtor(int obj, int a2); //no Entrypoint - comes from the object its' self.




        internal static int* Ptr_extern_sendto = (int*)0x007935A4;
        internal static int* Ptr_extern_recvfrom = (int*)0x007935AC;
        internal static int Entrypoint_extern_sendto = 0;
        internal static int Entrypoint_extern_recvfrom = 0;
        internal static double* Timer__cur_time = (double*)0x008379A8;
        internal static double* uptime = (double*)0x008379B0;
        internal static int* gmClient = (int*)0x008379E4;
        internal static byte* Device__m_bIsActiveApp = (byte*)0x00838197;
        internal static byte* Device__m_bTrackLeaveCalled = (byte*)0x0083819A;
        internal static int* Device__m_hWnd = (int*)0x008381A4;
        internal static int* GlobalEventHandler = (int*)0x00838374;
        internal static int* Smartbox = (int*)0x0083DA58;
        internal static int* CObjectMaint = (int*)0x00842ADC;
        internal static int* CPhysicsPart__player_iid = (int*)0x00844C08;
        internal static int* CPhysicsPart__player_object = (int*)0x00844D68;
        internal static int* CPlayerModule = (int*)0x0087119C;
        internal static int* ClientUISystem = (int*)0x00871354;
        internal static int* ClientFellowshipSystem = (int*)0x0087150C;
        internal static int* ClientAllegianceSystem = (int*)0x008715BC;
        internal static int* ClientCombatSystem = (int*)0x0087166C;
        internal static int* ACCWeenieObject__selectedID = (int*)0x00871E54;
        internal static int* ACCWeenieObject__prevRequest = (int*)0x00871ED0;
        internal static int* ACCWeenieObject__prevRequestObjectID = (int*)0x00871ED4;
        internal static double* ACCWeenieObject__prevRequestTime = (double*)0x00871ED8;

        internal static ushort* Device__m_DisplayPrefs_Resolution_Height = (ushort*)0x00818B64;
        internal static ushort* Device__m_DisplayPrefs_Resolution_Width = (ushort*)0x00818B66;
        internal static byte* Device__m_DisplayPrefs_LandscapeDetail = (byte*)0x0081FFA4;
        internal static byte* Device__m_DisplayPrefs_EnvironmentDetail = (byte*)0x0081FFA5;
        internal static uint* Device__m_DisplayPrefs_Landscape = (uint*)0x0081FFA8;
        internal static uint* Device__m_DisplayPrefs_Environment = (uint*)0x0081FFAC;
        internal static uint* Device__m_DisplayPrefs_SceneryDraw = (uint*)0x0081FFB0;
        internal static uint* Device__m_DisplayPrefs_LandscapeDraw = (uint*)0x0081FFB4;

        internal static int CBaseQualities { get { try { return *(int*)((*(int*)((*CPhysicsPart__player_object) + 0x012C)) + 0x014C) + 0x0038; } catch { return 0; } } }
        internal static int gmExternalContainerUI { get { try { return ResolveHandler(0x4DD1F9) - 1528; } catch { return 0; } } }
        internal static int gmExternalContainerUI_m_itemList { get { try { return *(int*)(gmExternalContainerUI + 0x060C); } catch { return 0; } } }

        [DllImport("kernel32.dll")] internal static extern bool VirtualProtectEx(IntPtr hProcess, IntPtr lpAddress, UIntPtr dwSize, int flNewProtect, out int lpflOldProtect);
        [DllImport("user32.dll")] internal static extern int GetForegroundWindow();
        #region ResolveHandler(int handlerID)
        internal static int ResolveHandler(int handlerID) {
            try {
                int m_handlers = *(int*)((*(int*)(*P.GlobalEventHandler + 4)) + 8 + ((handlerID % 0x17) << 2));
                while (*(int*)m_handlers != handlerID) m_handlers = *(int*)(m_handlers + 4);
                return *(int*)*(int*)(*(int*)(m_handlers + 8) + 4);
            } catch { return 0; }
        }
        #endregion
        #region Write(IntPtr address, <int,float,byte> newValue)
        internal static void Write(IntPtr address, int newValue) {
            try {
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.intsize, 0x40, out int b);
                *(int*)address = newValue;
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.intsize, b, out b);
            } catch { }
        }

        #endregion
        #region PatchCall(int callLocation, IntPtr newPointer)
        internal static bool PatchCall(int callLocation, IntPtr newPointer) {
            if (((*(byte*)callLocation) & 0xFE) != 0xE8)
                return false;
            int previousOffset = *(int*)(callLocation + 1);
            int previousPointer = previousOffset + (callLocation + 5);
            int newOffset = (int)newPointer - (callLocation + 5);
            Write((IntPtr)(callLocation + 1), newOffset);
            return true;
        }
        #endregion
        #region ReadCall(int callLocation)
        internal static int ReadCall(int callLocation) {
            if (((*(byte*)callLocation) & 0xFE) != 0xE8)
                return 0;
            int previousOffset = *(int*)(callLocation + 1);
            int previousPointer = previousOffset + (callLocation + 5);
            return previousPointer;
        }
        #endregion
        #region SwapBytes(<uint,ushort,short>)
        internal static short SwapBytes(short x) {
            return (short)((x >> 8) | (x << 8));
        }
        #endregion

        internal static System.Collections.Generic.List<Hooker> hookers = new System.Collections.Generic.List<Hooker>();
        public static void Cleanup() {
            for (int i = hookers.Count - 1; i > -1; i--)
                hookers[i].Remove();
        }
    }

    public class Hooker {
        internal IntPtr Entrypoint;
        internal Delegate Del;
        internal int call;
        internal bool Hooked = false;

        public Hooker(int entrypoint, int call_location) {
            Entrypoint = (IntPtr)entrypoint;
            call = call_location;
        }
        public void Setup(Delegate del) {
            if (!Hooked) {
                if (P.ReadCall(call) != (int)Entrypoint) {
                    Core.WriteToDebugLog($"Failed to detour 0x{call:X8}. expected 0x{(int)Entrypoint:X8}, received 0x{P.ReadCall(call):X8}");
                    return;
                }
                Hooked = true;
                Del = del;
                if (!P.PatchCall(call, Marshal.GetFunctionPointerForDelegate(Del))) {
                    Del = null;
                    Hooked = false;
                } else {
                    P.hookers.Add(this);
                    //Core.WriteToDebugLog($"Detouring {(int)Entrypoint:X8}");
                }
            }
        }
        public void Remove() {
            if (Hooked) {
                if (P.PatchCall(call, Entrypoint)) {
                    Del = null;
                    Hooked = false;
                    P.hookers.Remove(this);
                    //Core.WriteToDebugLog($"Un-detouring {(int)Entrypoint:X8}");
                }
            }
        }
    }

    public enum UIElement {
        SBOX = 0x1000049A, // SmartBox - 3d area
        CHAT = 0x10000601, // Chat
        FCH1 = 0x10000505, // rgFloatingChatWindows[0]
        FCH2 = 0x1000050E, // rgFloatingChatWindows[1]
        FCH3 = 0x1000050F, // rgFloatingChatWindows[2]
        FCH4 = 0x10000510, // rgFloatingChatWindows[3]
        EXAM = 0x100005F7, // examination ui
        VITS = 0x100005FA, // m_pStackedVitals
        SVIT = 0x100006D5, // m_pSideBySideVitals
        ENVP = 0x100005FD, // vendor/trade/loot window
        PANS = 0x100005FF, // inventory / options / etc
        TBAR = 0x10000603, // toolbar (shortcuts, backpack icon)
        INDI = 0x10000611, // link status / X / etc
        PBAR = 0x10000613, // jump bar
        COMB = 0x100006B5, // combat bar
        RADA = 0x100006D2, // m_pSmartBoxRadar
    }
    public enum NameType {
        NAME_SINGULAR = 0x0,
        NAME_PLURAL = 0x1,
        NAME_APPROPRIATE = 0x2,
    };

    public enum ConfirmationType {
        UNDEF = 0x0,
        ALLEGIANCE_SWEAR = 0x1,
        ALTER_SKILL = 0x2,
        ALTER_ATTRIBUTE = 0x3,
        FELLOWSHIP_RECRUIT = 0x4,
        CRAFT_INTERACTION = 0x5,
        USE_AUGMENTATION = 0x6,
        YESNO = 0x7,
    };

}
