using System.Reflection;
using System.Runtime.InteropServices;
[assembly: AssemblyTitle("UBHelper")]
[assembly: AssemblyDescription("Hack the Planet")]
[assembly: AssemblyCompany("Ellingson Mineral Company")]
[assembly: AssemblyProduct("UtilityBelt")]
[assembly: AssemblyCopyright("Copyright 1337 Zero Cool")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("0.0.0.1")]
[assembly: AssemblyFileVersion("0.0.0.1")]
[assembly: AssemblyInformationalVersion("0.0.0.1-test123")]
[assembly: Guid("c51788b5-3c43-471a-8034-79d1717bc8cb")]
