﻿using System;
using System.Runtime.InteropServices;

namespace UBHelper {
    /// <summary>
    /// Helper class for working with vendors
    /// </summary>
    public static class Vendor {
        internal static int _vendorId = 0;
        /// <summary>
        /// Id of the currently opened vendor. Will be 0 if no vendor is open.
        /// </summary>
        public static int Id { get => _vendorId; }
        internal static void Enable() {

            P.CM_Vendor__SendNotice_CloseVendor.Setup(new P.def_CM_Vendor__SendNotice_CloseVendor(Hook_CM_Vendor__SendNotice_CloseVendor));
            P.CM_Vendor__SendNotice_OpenVendor.Setup(new P.def_CM_Vendor__SendNotice_OpenVendor(Hook_CM_Vendor__SendNotice_OpenVendor));
            _vendorId = Core.VendorID;
        }
        internal static void Disable() {
            P.CM_Vendor__SendNotice_CloseVendor.Remove();
            P.CM_Vendor__SendNotice_OpenVendor.Remove();
        }
        /// <summary>
        /// Raised when the vendor window has been closed
        /// </summary>
        public static event EventHandler<EventArgs> VendorClosed;
        internal static char Hook_CM_Vendor__SendNotice_CloseVendor(int i_bUpdating) {
            VendorClosed?.Invoke(null, new EventArgs());
            _vendorId = 0;
            return P.Call_CM_Vendor__SendNotice_CloseVendor(i_bUpdating);
        }
        internal static char Hook_CM_Vendor__SendNotice_OpenVendor(int i_vendorID, int i_vendorProfile, int i_itemProfileList, int i_startMode) {
            _vendorId = i_vendorID;
            return P.Call_CM_Vendor__SendNotice_OpenVendor(i_vendorID, i_vendorProfile, i_itemProfileList, i_startMode);
        }
    }
}
