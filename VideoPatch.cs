﻿using System;
using Decal.Adapter;

namespace UBHelper {
    /// <summary>
    /// Controls enabling/disabling the 3d rendering window of the game client.
    /// Useful for lowering cpu consumption.
    /// </summary>
    public static class VideoPatch {
        internal static bool _enabled = false;
        internal static bool invokeChanged = false;
        internal static bool _bgOnly = false;

        /// <summary>
        /// Disables rendering the 3d portion of the game client.
        /// </summary>
        public static bool Enabled {
            get => _enabled;
            set {
                if (value == _enabled) return;
                _enabled = value;
                if (value) {
                    if (P.GetForegroundWindow() != Core.Client_HWND) P.Call_Device__Deactivate();
                    P.SmartBox__Draw.Setup(new P.def_SmartBox__Draw(Hook_SmartBox__Draw));
                    if (_bgOnly) Decal.Adapter.CoreManager.Current.WindowMessage += Core_WindowMessage_VideoPatchFocusToggle;
                } else {
                    P.SmartBox__Draw.Remove();
                    if (_bgOnly) Decal.Adapter.CoreManager.Current.WindowMessage -= Core_WindowMessage_VideoPatchFocusToggle;
                }
                Changed?.Invoke();
            }
        }

        /// <summary>
        /// Enables rendering the 3d portion of the game client while the client has focus. (requires UBHelper.VideoPatch.Enabled == true)
        /// </summary>
        public static bool bgOnly {
            get => _bgOnly;
            set {
                if (value == _bgOnly) return;
                _bgOnly = value;
                if (value) Decal.Adapter.CoreManager.Current.WindowMessage += Core_WindowMessage_VideoPatchFocusToggle;
                else Decal.Adapter.CoreManager.Current.WindowMessage -= Core_WindowMessage_VideoPatchFocusToggle;
                Changed?.Invoke();
            }
        }

        internal static void Hook_SmartBox__Draw(int SmartBox) {
            if (bgOnly && Core.isFocused)
                P.Call_SmartBox__Draw(SmartBox);
            else {
                P.Call_SmartBox__SetNormalMode(SmartBox);
                P.Call_SmartBox__update_viewer(SmartBox);
            }

            if (invokeChanged) { Changed?.Invoke(); invokeChanged = false; }
        }

        private static void Core_WindowMessage_VideoPatchFocusToggle(object sender, WindowMessageEventArgs e) {
            switch (e.Msg) {
                case 0x0007: // WM_SETFOCUS
                case 0x0021: // WM_MOUSEACTIVATE
                case 0x0086: // WM_NCACTIVATE
                    invokeChanged = true;
                    break;
                case 0x0008: // WM_KILLFOCUS
                    invokeChanged = true;
                    break;

                //case 0x0200: //MOUSE MOVE - called once when the mouse is first over the client (leave for future use)
                //    if (*P.Device__m_bTrackLeaveCalled == 0)
                //        invokeChanged = true;
                //    break;
                case 0x0201:
                case 0x0204:
                case 0x0207:
                case 0x020B:
                    if (!Core.isFocused)
                        P.Call_Device_Activate();
                    break;
                //case 0x02A3: // MOUSE OUT - called once when the mouse is no longer over the client (leave for future use)
                //    invokeChanged = true;
                //    break;


                default:
                    break;
            }
        }
        /// <summary>
        /// Raised when VideoPatch has been enabled/disabled
        /// </summary>
        public static event Core.callback Changed;
    }
}
